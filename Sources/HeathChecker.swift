//
//  HeathChecker.swift
//  manhealth
//
//  Created by Michael Sanford on 9/6/17.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation
import SwiftOnSockets
import MancalaCore
import NetworkingCore
import MancalaGameEngine
import Dispatch

fileprivate let domain = "HealthChecker"

enum HealthCheckError: Error {
    case attemptToStartCheckWhileAlreadyInProgress
    case lobbyConnectionFailure(String)
    case internalError
}

enum Result<T> {
    case success(T)
    case failure(Error)
}

class HealthChecker: MancalaNetworkClientDelegate {
    fileprivate var networkClient: MancalaNetworkClient?
    fileprivate var completionHandler: ((Result<Void>) -> ())?
    private(set) var didConnectToLobby: Bool = false
    private(set) var lobbyResponseTime: Timestamp32?
    private(set) var didConnectToGameServer: Bool = false
    private(set) var gameServerResponseTime: Timestamp32?

    func startHealthCheck(_ appVersion: String, completion: @escaping (Result<Void>) -> ()) {
        completionHandler = completion
        do {
            try startOnlineSession(appVersion)
            networkClient?.startJoinGameWorkflow()
        } catch {
            DispatchQueue.main.async { [weak self] in
                self?.onDidComplete(withError: error)
            }
        }
    }
    
    fileprivate func onDidComplete(withError error: Error?) {
        networkClient?.disconnect()
        networkClient = nil
        guard let handler = completionHandler else { return }
        completionHandler = nil
        let result: Result<Void>
        if let error = error {
            result = .failure(error)
        } else {
            result = .success()
//            result = .failure(HealthCheckError.lobbyConnectionFailure("timeout"))
        }
        handler(result)
    }
    
    func startOnlineSession(_ appVersion: String) throws {
        guard networkClient == nil else { throw HealthCheckError.attemptToStartCheckWhileAlreadyInProgress }
        
        let playerInfo = MancalaPlayerInfo(
            playerName: "__fs5.health_checker",
            gameboardID: "mancala.theme.wood",
            points: 2000,
            numberOfGamesPlayed: 1
        )
        
//        let lobbyAddress = IPAddress(formatted: "208.52.182.216:8189")
        //        let lobbyAddress = IPAddress(formatted: "192.168.1.195:8189")
        guard let networkClient = MancalaNetworkClient(appVersion: appVersion, playerInfo: playerInfo) else {
            log(.warning, domain, "failed to create network client")
            return
        }
        
        self.networkClient = networkClient
        networkClient.delegate = self
    }
    
    func onDidReceiveLobbyAddress(with address: IPAddress) {
    }
    
    func onDidReceiveChatMessage(_ message: ChatMessage, fromPlayer: PlayerIdentity) {
    }
    
    func onDidConnectToLobby() {
//        print("onDidConnectToLobby")
        didConnectToLobby = true
        print("[+]   Lobby: connected")
        do {
            try networkClient?.lobbyConnection?.sendPing { [weak self] in
                self?.lobbyResponseTime = $0
                print("[+]   Lobby: response = \($0)ms")
            }
        } catch {
            print("[+]   Lobby: ping failed. \(error)")
        }
    }
    
    func onDidDisconnectFromLobby(withError error: Error?) {
        if let error = error, networkClient != nil {
            onDidComplete(withError: error)
        } else {
            //print("onDidDisconnectFromLobby")
        }
    }
    
    func onDidFindGame(withMyIdentity myIdentity: PlayerIdentity, opponentInfo: MancalaPlayerInfo, startingPlayer: PlayerIdentity) {
//        print("onDidFindGame")
    }
    
    func onDidNotFindGame(error: String) {
        onDidComplete(withError: HealthCheckError.lobbyConnectionFailure(error))
    }
    
    // Game Server callbacks
    func onDidConnectToGameServer() {
//        print("onDidConnectToGameServer")
        didConnectToGameServer = true
        print("[+]   Game server: connected")
        do {
            try networkClient?.gameServerConnection?.sendPing { [weak self] in
                self?.gameServerResponseTime = $0
                print("[+]   Game server: response = \($0)ms")
            }
        } catch {
            print("[+]   Game server: ping failed. \(error)")
        }
    }
    
    func onDidDisconnectFromGameServer(withError error: Error?) {
        if let error = error, networkClient != nil {
            onDidComplete(withError: error)
        } else {
            //print("onDidDisconnectFromGameServer")
        }
    }
    
    func onDidStartGame(withStartingPlayer startingPlayer: PlayerIdentity, gameboardID: String) {
        onDidComplete(withError: nil)
    }
    
    func onDidMakeMove(with pitID: PitID, byPlayer identity: PlayerIdentity) {
        print("onDidMakeMove")
    }
    
    func onDidStartTurnCountdown(withSeconds: Int, player: PlayerIdentity) {
        print("onDidStartTurnCountdown")
    }
    
    //    func onDidTimeoutTurn(byPlayer identity: PlayerIdentity)
    func onDidSkipTurn(byPlayer identity: PlayerIdentity) {
        print("onDidSkipTurn")
    }
    
    func onDidEarn(points: Int, coins: Int) {
        print("onDidEarn")
    }
    
    func onDidJoinGame(withPlayer playerIdentity: PlayerIdentity) {
        print("onDidJoinGame")
    }
    
    func onDidLeaveGame(withPlayer playerIdentity: PlayerIdentity) {
        print("onDidLeaveGame")
    }
    
    func onDidInteractWithProp(withProp propID: String, interactionType: Int, byPlayer: PlayerIdentity) {
        print("onDidInteractWithProp")
    }
}
