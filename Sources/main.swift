//
//  main.swift
//  manhealth
//
//  Created by Michael Sanford on 9/6/17.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation
import SwiftOnSockets
import MancalaGameEngine

struct ChatMessage {
    let message: String
}

@discardableResult
func shell(launchPath: String, arguments: [String]) -> String {
    let task = Process()
    task.launchPath = launchPath
    task.arguments = arguments
    
    let pipe = Pipe()
    task.standardOutput = pipe
    task.launch()
    
    let data = pipe.fileHandleForReading.readDataToEndOfFile()
    let output: String = String(data: data, encoding: .utf8)!
    return output
}

//guard CommandLine.arguments.count == 2 else {
//    print("Usage: manhealth <ip_address:port>")
//    exit(-1)
//}

//guard let ipAddress = IPAddress(formatted: CommandLine.arguments[1]) else {
//    print("Usage: manhealth <ip_address:port>")
//    print("ERROR: invalid <ip_address:port>")
//    exit(-1)
//}

let appVersion: String
if CommandLine.argc > 1 {
    appVersion = CommandLine.arguments[1]
} else {
    appVersion = "<latest_version>"
}

switch appVersion {
case "11.1.2", "11.2.0":
    MancalaNetworkProtocolVersion.current = .v1_0
default:
    MancalaNetworkProtocolVersion.current = MancalaNetworkProtocolVersion.latest
}

let formatter = DateFormatter()
formatter.dateStyle = .short
formatter.timeStyle = .medium

print("[+] Health Check: Started [\(appVersion)]")

let checker = HealthChecker()
checker.startHealthCheck(appVersion) {
    switch $0 {
    case .success:
        print("[+] Health Check: Success [\(formatter.string(from: Date()))]")
        exit(0)
    case .failure:
        let errorString: String
        if !checker.didConnectToLobby {
            errorString = "lobby connection failed"
        } else if checker.lobbyResponseTime == nil {
            errorString = "lobby is unresponsive"
        } else if !checker.didConnectToGameServer {
            errorString = "game server connection failed"
        } else if checker.gameServerResponseTime == nil {
            errorString = "game server is unresponsive"
        } else {
            errorString = "unknown error"
        }
        print("[+] Health Check: Failed [\(formatter.string(from: Date()))] - \(errorString)")
        NetworkService.shared.sendSMS(errorString) {
            exit(-1)
        }
    }
}

CFRunLoopRun()
