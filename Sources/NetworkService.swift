//
//  NetworkService.swift
//  Mancala-iOS
//
//  Created by Michael Sanford on 7/27/17.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation
import NetworkingCore
import Dispatch

fileprivate let domain = "NetworkService"

public class NetworkService {
    public static let shared = NetworkService()
    fileprivate let authorization = "ACf2d6c911ea7866b46bcd94a8756c9af0:0746574ea8e6e03a93109950626375e5"
    
    enum RequestError: Error {
        case parseError(String)
        case invalidJsonBody
        case internalError
    }
    
    enum ResponseError: Error {
        case invalidResponse(String)
        case problematicStatusCode(Int)
        case invalidJson(String)
        case emptyResult
        case unsuccess
        case internalError
    }
    
    enum APIResponseResult<T> {
        case failure(Error)
        case unsuccess(T)
        case success(T)
    }
    
    fileprivate enum RequestType {
        case get, post, put
    }
    
    fileprivate struct API {
        let endpoint: String
        let params: [String: String]?
        let requestType: RequestType
        let body: [String: Any]? //json
        let authorization: Bool
        
        private init(getEndpoint: String, params: [String: String]? = nil) {
            self.endpoint = getEndpoint
            self.params = params
            self.requestType = .get
            self.body = nil
            self.authorization = false
        }
        
        private init(endpoint: String, params: [String: String]? = nil, requestType: RequestType, body: [String: Any]? = nil, authorization: Bool = false) {
            self.endpoint = endpoint
            self.params = params
            self.requestType = requestType
            self.body = body
            self.authorization = authorization
        }
        
        static func message(_ message: String) -> API {
//            curl 'https://api.twilio.com/2010-04-01/Accounts/ACf2d6c911ea7866b46bcd94a8756c9af0/Messages.json' -X POST --data-urlencode 'To=+17039634619' --data-urlencode 'From=+12408835656' --data-urlencode 'Body=this is a test' -u ACf2d6c911ea7866b46bcd94a8756c9af0:0746574ea8e6e03a93109950626375e5
            let fullMessage: String
            if let appVersion = MancalaNetworkClient.appVersion {
                fullMessage = "[\(appVersion)] \(message)"
            } else {
                fullMessage = "[???] \(message)"
            }
            return API(
                endpoint: "Messages.json",
                requestType: .post,
                body: [
                    "To": "+17039634619",
                    "From": "+12408835656",
                    "Body": fullMessage
                ],
                authorization: true
            )
        }
        
        var bodyString: String {
            guard let body = body else { return "" }
            return body.reduce("") { result, pair in
                guard let valueString = pair.value as? String else { return result }
                let pairString = "\(pair.key)=\(valueString)"
                return result.count == 0 ? pairString : "\(result)&\(pairString)"
            }
        }
    }
    
    private let urlSession: URLSession = {
        let config = URLSessionConfiguration.default
        
        return URLSession(configuration: config, delegate: nil, delegateQueue: nil)
    }()
    
    fileprivate var apiURLString: String {
        return "https://api.twilio.com/2010-04-01/Accounts/ACf2d6c911ea7866b46bcd94a8756c9af0"
    }
    
    private func performAPIRequest(_ api: API, completion: @escaping (APIResponseResult<[String: Any]>) -> ()) throws {
        let validRequest = try validatedUrlRequestResult(for: api)
        
        let session = urlSession.dataTask(with: validRequest) { [weak self] (data, response, error) in
            guard let strongSelf = self else { return }
            let responseResult = strongSelf.validatedURLResponseResult(for: api, data: data, response: response)
            switch responseResult {
            case .failure(let error):
                //                if let response = response {
                //                    print("response=\(response)")
                //                }
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            case .unsuccess(let json, _):
                //                if let response = response {
                //                    print("response=\(response)")
                //                }
                DispatchQueue.main.async {
                    completion(.unsuccess(json))
                }
            case .success(let json, _):
                DispatchQueue.main.async {
                    completion(.success(json))
                }
            }
        }
        session.resume()
    }
    
    func sendSMS(_ message: String, completion: @escaping () -> ()) {
        do {
            let api = API.message(message)
            try performAPIRequest(api) { result in
                switch result {
                case .failure(let error):
                    print("failed to send sms: \(error)")
                case .unsuccess, .success:
                    break
                }
                completion()
            }
        } catch {
            print("error=\(error)")
        }
    }
}

extension NetworkService.API {
    fileprivate var queryString: String {
        guard let params = params else { return "" }
        var isFirst = true
        return params.reduce("") { result, keyValuePair in
            let query = "\(keyValuePair.key)=\(keyValuePair.value)"
            let prefix = isFirst ? "?" : "&"
            isFirst = false
            return "\(result)\(prefix)\(query)"
        }
    }
}

extension NetworkService {
    
    fileprivate func validatedURLResponseResult(for api: API, data: Data?, response: URLResponse?) -> APIResponseResult<([String: Any], HTTPURLResponse)> {
        guard let httpResponse = response as? HTTPURLResponse else { return .failure(ResponseError.internalError) }
        guard let data = data else {
            return .failure(ResponseError.invalidResponse("missing success property"))
        }
        
        do {
            guard let json: [String: Any] = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                return .failure(ResponseError.invalidResponse("Could not create json as a dictionary"))
            }
            if let errorCode = json["error_code"], !(errorCode is NSNull) {
                return .failure(ResponseError.invalidResponse("error code=\(errorCode)"))
            }
            return .success((json, httpResponse))
        } catch {
            return .failure(ResponseError.invalidResponse("Could not parse response error=\(error)"))
        }
    }
    
    fileprivate func validatedUrlRequestResult(for api: API) throws -> URLRequest {
        guard let requestURL: URL = URL(string: "\(apiURLString)/\(api.endpoint)\(api.queryString)") else {
            throw NetworkService.RequestError.parseError("Failed to make valid endpoint")
        }
        
        var request: URLRequest = URLRequest(url: requestURL)
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        if api.authorization {
            let base64: String = Data(authorization.utf8).base64EncodedString()
            request.addValue("Basic \(base64)", forHTTPHeaderField: "Authorization")
        }
        
        switch api.requestType {
        case .get:
            request.httpMethod = "GET"
        case .post:
            request.httpMethod = "POST"
        case .put:
            request.httpMethod = "PUT"
        }
        
        let body: [String: Any] = api.body ?? [:]
        guard !body.isEmpty else { return request }
        
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = api.bodyString.data(using: .utf8)
        return request
    }
}
