//
//  MancalaGameClient.swift
//  Mancala
//
//  Created by Michael Sanford on 12/23/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import SwiftOnSockets
import NetworkingCore
import GamingCore
import MancalaCore
import MancalaGameEngine
import MancalaLobbyCore
import Dispatch

protocol MancalaNetworkClientDelegate: class {
    
    // Lobby callbacks
    func onDidReceiveLobbyAddress(with address: IPAddress)
    func onDidConnectToLobby()
    func onDidDisconnectFromLobby(withError error: Error?)
    
    func onDidFindGame(withMyIdentity myIdentity: PlayerIdentity, opponentInfo: MancalaPlayerInfo, startingPlayer: PlayerIdentity)
    func onDidNotFindGame(error: String)
    
    // Game Server callbacks
    func onDidConnectToGameServer()
    func onDidDisconnectFromGameServer(withError error: Error?)
    
    func onDidStartGame(withStartingPlayer startingPlayer: PlayerIdentity, gameboardID: String)
    func onDidMakeMove(with pitID: PitID, byPlayer identity: PlayerIdentity)
    func onDidStartTurnCountdown(withSeconds: Int, player: PlayerIdentity)
    //    func onDidTimeoutTurn(byPlayer identity: PlayerIdentity)
    func onDidSkipTurn(byPlayer identity: PlayerIdentity)
    func onDidEarn(points: Int, coins: Int)
    
    func onDidJoinGame(withPlayer playerIdentity: PlayerIdentity)
    func onDidLeaveGame(withPlayer playerIdentity: PlayerIdentity)
    
    func onDidInteractWithProp(withProp propID: String, interactionType: Int, byPlayer: PlayerIdentity)
    func onDidReceiveChatMessage(_ message: ChatMessage, fromPlayer: PlayerIdentity)
}

enum MancalaNetworkClientError: Error {
    case missingGameToken
    case internalError
    case timeout
}

fileprivate let domain = "MancalaNetworkClient"

fileprivate protocol NetOperationQueueService: class {
    func connectToLobby()
    func sendLobbyMessage(_ message: Message)
    func connectToGameServer(withIPAddress ipAddress: IPAddress)
    func sendGameServerMessage(_ message: Message)
}

class MancalaNetworkClient: NetOperationQueueService {
    
    fileprivate enum State {
        case connectingToLobby
        case connectedToLobby
        case requestingGameFromLobby(UUID)
        case requestedGameFromLobby(IPAddress, GameToken)
        case connectingToGameServer(IPAddress, GameToken)
        case connectedToGameServer(GameToken)
        case joiningGameServer(GameToken, UUID)
        case joinedGameServer(GameToken)
        case disconnecting
        case disconnected
        
        var isActive: Bool {
            switch self {
            case .disconnecting, .disconnected: return false
            default: return true
            }
        }
    }
    
    static var serverAddress: IPAddress!
    static var appVersion: String!
    private static var serverAddressRequestInProgress: Bool = false
    private static let serverRequestQueue = DispatchQueue(label: "server.address")
    private static var serverAddressCompleitonHandlers: [(Result<IPAddress>) -> ()] = []
    private static var serverAddressStartTimestamp: Timestamp32 = 0
    static func requestServerAddress(completion: @escaping (Result<IPAddress>) -> ()) {
        
        func onDidComplete(_ result: Result<IPAddress>) {
            serverRequestQueue.sync {
                serverAddressRequestInProgress = false
                log(.debug, domain, "lobby address response time = \(serverAddressStartTimestamp.millisecondsSinceNow) ms")
                serverAddressStartTimestamp = 0
            }
            
            DispatchQueue.main.async {
                completion(result)
                serverAddressCompleitonHandlers.forEach { $0(result) }
                serverAddressCompleitonHandlers = []
            }
        }
        
        var foundAddress: IPAddress?
        var isRequestAllowed: Bool = false
        serverRequestQueue.sync {
            foundAddress = MancalaNetworkClient.serverAddress
            if foundAddress == nil && !MancalaNetworkClient.serverAddressRequestInProgress {
                isRequestAllowed = true
                serverAddressRequestInProgress = true
                serverAddressStartTimestamp = Date.now32
            }
        }
        
        if isRequestAllowed {
            guard let url = URL(string: "https://s3.amazonaws.com/files-flipside5/mancala-prod-address.json") else {
                onDidComplete(Result.failure(NSError(domain: domain, code: -1, userInfo: [NSLocalizedDescriptionKey: "internal error for lobby address"])))
                return
            }
            let urlSession = URLSession(configuration: URLSessionConfiguration.ephemeral, delegate: nil, delegateQueue: nil)
            let request: URLRequest = URLRequest(url: url)
            let session = urlSession.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                    guard let error = error else {
                        onDidComplete(.failure(NSError(domain: domain, code: -2, userInfo: [NSLocalizedDescriptionKey: "failed to make lobby address http request"])))
                        return
                    }
                    onDidComplete(.failure(error))
                    return
                }
                
                let lobbyAddressString: String
                let lobbyVersion: String
                do {
                    guard let lobbyAddresesJSON = try JSONSerialization.jsonObject(with: data, options: []) as? [String: String],
                        !lobbyAddresesJSON.isEmpty else {
                            onDidComplete(.failure(NSError(domain: domain, code: -3, userInfo: [NSLocalizedDescriptionKey: "lobby address result is invalid"])))
                            return
                    }
                    if let value = lobbyAddresesJSON[MancalaNetworkClient.appVersion] {
                        lobbyAddressString = value
                        lobbyVersion = MancalaNetworkClient.appVersion
                    } else {
                        let allAddresses = lobbyAddresesJSON.keys.sorted()
                        guard let value = allAddresses.last else {
                            // this should never happen
                            onDidComplete(.failure(NSError(domain: domain, code: -3, userInfo: [NSLocalizedDescriptionKey: "lobby address result is invalid(2)"])))
                            return
                        }
                        
                        lobbyVersion = value
                        lobbyAddressString = lobbyAddresesJSON[lobbyVersion]!
                        MancalaNetworkClient.appVersion = value
                    }
                    print("[+] lobby version=\(lobbyVersion) | address=\(lobbyAddressString)")
                } catch {
                    onDidComplete(.failure(NSError(domain: domain, code: -3, userInfo: [NSLocalizedDescriptionKey: "failed to decode lobby address response"])))
                    return
                }
                
                guard let address = IPAddress(formatted: lobbyAddressString) else {
                    onDidComplete(.failure(NSError(domain: domain, code: -4, userInfo: [NSLocalizedDescriptionKey: "invalid ip address string \(lobbyAddressString)"])))
                    return
                }
                
                MancalaNetworkClient.serverAddress = address
                onDidComplete(.success(address))
            }
            session.resume()
            return
        } else if let serverAddress = foundAddress {
            onDidComplete(.success(serverAddress))
        } else {
            serverAddressCompleitonHandlers.append(completion)
        }
    }
    
    weak var delegate: MancalaNetworkClientDelegate?
    fileprivate var lobbyAddress: IPAddress? //= IPAddress(formatted: "192.168.1.195:8189")
    var lobbyConnection: ServiceConnection?
    var gameServerConnection: ServiceConnection?
    fileprivate var gameToken: GameToken?
    
    fileprivate enum NetOperation {
        case connectToLobby
        case requestGameFromLobby(UUID, Message)
        case connectToGameServer(IPAddress, GameToken)
        case joinGameServer(GameToken, UUID, Message)
    }
    
    fileprivate struct NetOperationQueue {
        var state = State.disconnected
        
        private var queue = Queue<NetOperation>()
        private let service: NetOperationQueueService
        fileprivate var isOperationInProgress: Bool = false
        fileprivate var didStartOperationHandler: ((State) -> ())?
        fileprivate var didCompleteOperationHandler: ((State) -> ())?
        
        init(service: NetOperationQueueService) {
            self.service = service
        }
        
        mutating func performNextOperation() {
            guard !isOperationInProgress, let nextOp = queue.remove() else { return }
            
            switch nextOp {
            case .connectToLobby:
                guard !state.isActive else {
                    assert(false, "attempt to connect to lobby with an invalid state")
                    return
                }
                isOperationInProgress = true
                state = .connectingToLobby
                DispatchQueue.global().async { [weak service] in
                    service?.connectToLobby()
                }
            case .requestGameFromLobby(let requestID, let message):
                guard case .connectedToLobby = state else {
                    assert(false, "attempt to request game from lobby with an invalid state")
                    return
                }
                isOperationInProgress = true
                state = .requestingGameFromLobby(requestID)
                service.sendLobbyMessage(message)
            case .joinGameServer(let gameToken, let requestID, let message):
                guard case .connectedToGameServer = state else {
                    assert(false, "attempt to join game server with an invalid state")
                    return
                }
                isOperationInProgress = true
                state = .joiningGameServer(gameToken, requestID)
                service.sendGameServerMessage(message)
            case .connectToGameServer(let ipAddress, let gameToken):
                guard case .requestedGameFromLobby = state else {
                    assert(false, "attempt to connectToGameServer with an invalid state")
                    return
                }
                isOperationInProgress = true
                state = .connectingToGameServer(ipAddress, gameToken)
                service.connectToGameServer(withIPAddress: ipAddress)
            }
            
            if isOperationInProgress {
                didStartOperationHandler?(state)
            }
        }
        
        mutating func addRequestGameFromLobbyOperation(withRequestID requestID: UUID, message: Message) {
            let op = NetOperation.requestGameFromLobby(requestID, message)
            queue.add(op)
            performNextOperation()
        }
        
        mutating func addJoinGameServerOperation(withGameToken gameToken: GameToken, requestID: UUID, message: Message) {
            let op = NetOperation.joinGameServer(gameToken, requestID, message)
            queue.add(op)
            performNextOperation()
        }
        
        mutating func onDidConnectToLobby() {
            guard case .connectingToLobby = state else {
                assert(false, "onDidConnectToLobby called in invalid state")
                return
            }
            isOperationInProgress = false
            state = .connectedToLobby
            didCompleteOperationHandler?(state)
            performNextOperation()
        }
        
        mutating func onDidConnectToGameServer() {
            guard case .connectingToGameServer(_, let gameToken) = state else {
                assert(false, "onDidConnectToGameServer called in invalid state")
                return
            }
            isOperationInProgress = false
            state = .connectedToGameServer(gameToken)
            didCompleteOperationHandler?(state)
            performNextOperation()
        }
        
        mutating func onDidReceiveGameResponseFromLobby(withGameServerAddress address: IPAddress, gameToken: GameToken) {
            guard case .requestingGameFromLobby = state else {
                assert(false, "onDidReceiveGameResponseFromLobby called in invalid state")
                return
            }
            isOperationInProgress = false
            state = .requestedGameFromLobby(address, gameToken)
            didCompleteOperationHandler?(state)
            performNextOperation()
        }
        
        mutating func onDidJoinGameServer() {
            guard case .joiningGameServer(let gameToken, _) = state else {
                assert(false, "onDidJoinGameServer called in invalid state")
                return
            }
            isOperationInProgress = false
            state = .joinedGameServer(gameToken)
            didCompleteOperationHandler?(state)
            performNextOperation()
        }
        
        mutating func onDidDisconnect() {
            guard case .disconnecting = state else {
                assert(false, "onDidDisconnect called in invalid state")
                return
            }
            isOperationInProgress = false
            state = .disconnected
            didCompleteOperationHandler?(state)
            performNextOperation()
        }
        
        mutating func addConnectToLobbyOperation() {
            guard !state.isActive else { return }
            queue.add(.connectToLobby)
            performNextOperation()
        }
        
        mutating func addConnectToGameServerOperation(withAddress address: IPAddress, gameToken: GameToken) {
            queue.add(.connectToGameServer(address, gameToken))
            performNextOperation()
        }
    }
    fileprivate var operationQueue: NetOperationQueue!
    fileprivate var lobbyDelegate: LobbyDelegate!
    fileprivate var gameServerDelegate: GameServerDelegate!
    fileprivate var operationTimeoutWorkItem: DispatchWorkItem?
    fileprivate var playerInfo: MancalaPlayerInfo
    
    init?(appVersion: String, playerInfo: MancalaPlayerInfo) {
        MancalaNetworkClient.appVersion = appVersion
        self.playerInfo = playerInfo
        lobbyDelegate = LobbyDelegate(client: self)
        gameServerDelegate = GameServerDelegate(client: self)
        operationQueue = NetOperationQueue(service: self)
        operationQueue.didStartOperationHandler =  onDidStartOperation
        operationQueue.didCompleteOperationHandler = onDidCompleteOperation
    }
    
    private func onDidStartOperation(state: State) {
        assert(operationTimeoutWorkItem == nil, "expected nil operation timeout work item when setting one")
        operationTimeoutWorkItem = DispatchQueue.main.addOperation(15) { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.operationTimeoutWorkItem = nil
            strongSelf.delegate?.onDidNotFindGame(error: "timeout")
        }
    }
    
    private func onDidCompleteOperation(state: State) {
        operationTimeoutWorkItem?.cancel()
        operationTimeoutWorkItem = nil
    }
    
    fileprivate func connectToLobby() {
        do {
            guard let lobbyAddress = lobbyAddress, let lobbyConnection = ServiceConnection(type: .tcp, remoteAddress: lobbyAddress) else {
                assert(false, "failed to create lobby ServiceConnection")
                return
            }
            lobbyConnection.delegate = lobbyDelegate
            self.lobbyConnection = lobbyConnection
            try lobbyConnection.connect()
        } catch {
            log(.warning, domain, "error durring connectToLobby; error=\(error)")
        }
    }
    
    fileprivate func connectToGameServer(withIPAddress ipAddress: IPAddress) {
        do {
            gameServerConnection = ServiceConnection(type: .udp, remoteAddress: ipAddress)
            gameServerConnection?.delegate = gameServerDelegate
            try gameServerConnection?.connect()
        } catch {
            log(.warning, domain, "error durring connectToGameServer (\(ipAddress)); error=\(error)")
        }
    }
    
    fileprivate func sendLobbyMessage(_ message: Message) {
        do {
            guard let lobbyConnection = lobbyConnection else { return }
            try lobbyConnection.send(message)
        } catch {
            log(.warning, domain, "error durring requestGameFromLobby; error=\(error)")
        }
    }
    
    fileprivate func sendGameServerMessage(_ message: Message) {
        do {
            try gameServerConnection?.send(message)
        } catch {
            log(.warning, domain, "error durring requestGameFromLobby; error=\(error)")
        }
    }
    
    fileprivate func disconnectFromLobby() {
        do {
            try lobbyConnection?.disconnect()
            lobbyConnection = nil
        } catch {
            log(.warning, domain, "error during lobby disconnect; error=\(error)")
        }
    }
    
    func disconnect() {
        do {
            try lobbyConnection?.disconnect()
            try gameServerConnection?.disconnect()
            onDidCompleteOperation(state: State.disconnected)
        } catch {
            log(.warning, domain, "error during disconnect; error=\(error)")
        }
    }
    
    /// MARK: Lobby requests
    
    private func startLobbyConnection() {
        switch operationQueue.state {
        case .disconnected, .disconnecting:
            operationQueue.addConnectToLobbyOperation()
        case .connectingToLobby:
            ()
        default:
            return
        }
        
        let request = PlayerNewGameRequest(playerInfo: playerInfo)
        let message = Message(type: ClientToLobbyMessageType.createGameRequest.rawValue, payload: ByteArchiver.archive(for: request))
        operationQueue.addRequestGameFromLobbyOperation(withRequestID: request.identifier, message: message)
    }
    
    func startJoinGameWorkflow() {
        if let address = lobbyAddress {
            delegate?.onDidReceiveLobbyAddress(with: address)
            startLobbyConnection()
            return
        }
        
        MancalaNetworkClient.requestServerAddress() { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let address):
                strongSelf.lobbyAddress = address
                strongSelf.delegate?.onDidReceiveLobbyAddress(with: address)
                strongSelf.startLobbyConnection()
            case .failure(let error):
                strongSelf.delegate?.onDidNotFindGame(error: "failed to find lobby server address: code=\(error.localizedDescription)")
            }
        }
    }
    
    /// MARK: Game Server Messages
    func sendDidInteractWithProp(named propID: String, interactionType: Int) throws {
        guard let identity = playerIdentity else { throw MancalaNetworkClientError.missingGameToken }
        let didInteractWithPropMessage = PlayerDidInteractWithPropMessage(playerIdentity: identity, propID: propID, interactionType: interactionType)
        try sendGameMessage(withType: PlayerDidInteractWithPropMessage.clientToServerType, payload: didInteractWithPropMessage)
    }
    
//    func sendChatMessage(_ message: ChatMessage) throws {
//        guard let identity = playerIdentity else { throw MancalaNetworkClientError.missingGameToken }
//        let payload = PlayerDidSendChatPayload(fromPlayer: identity, message: message.rawValue)
//        try sendGameMessage(withType: PlayerDidSendChatPayload.clientToServerType, payload: payload)
//    }
    
    func sendDidSkipTurn() throws {
        guard let identity = playerIdentity else { throw MancalaNetworkClientError.missingGameToken }
        let didSkipTurn = PlayerDidSkipTurn(playerIdentity: identity)
        try sendGameMessage(withType: PlayerDidSkipTurn.clientToServerType, payload: didSkipTurn)
    }
    
    func sendDidMakeMove(with pitID: PitID) throws {
        guard let identity = playerIdentity else { throw MancalaNetworkClientError.missingGameToken }
        let didMakeMoveMessage = PlayerDidRequestMoveMessage(playerIdentity: identity, pitID: pitID)
        try sendGameMessage(withType: PlayerDidRequestMoveMessage.type, payload: didMakeMoveMessage)
    }
    
    func sendRequestToPlayAgain() throws {
        guard let identity = playerIdentity else { throw MancalaNetworkClientError.missingGameToken }
        let playAgainMessage = PlayerDidRequestToPlayAgainMessage(playerIdentity: identity)
        try sendGameMessage(withType: PlayerDidRequestToPlayAgainMessage.type, payload: playAgainMessage)
    }
    
    private func sendGameMessage(withType type: MancalaClientToGameServerMessageType, payload: ByteCoding?) throws {
        guard let gameToken = gameToken, let connection = gameServerConnection else { throw MancalaNetworkClientError.missingGameToken }
        let payloadMessage = Message(type: type, payload: payload)
        let gameMessage = GameMessage(gameToken: gameToken, message: payloadMessage)
        let message = Message(gameMessage: gameMessage)
        try connection.send(message)
        log(.debug, domain, "did send sendGameMessage message (type=\(type))")
    }
    
    // TODO: should be in a generic game client
    private func sendStream(_ data: Data) throws {
        guard let gameToken = gameToken, let connection = gameServerConnection else {
            assert(false, "attempt to sendStream without a game token or connection")
            return
        }
        
        let archiver = ByteArchiver()
        archiver.encode(gameToken)
        let payload = archiver.archive + data
        let streamData = StreamData(version: 1, payload: payload)
        try connection.send(streamData)
    }
    
    var playerIdentity: PlayerIdentity? {
        guard let playerID = gameToken?.playerID else { return nil }
        return self.playerIdentity(forPlayerID: playerID)
    }
    
    fileprivate func playerIdentity(forPlayerID playerID: PlayerID) -> PlayerIdentity {
        return playerID == 0 ? .one : .two
    }
}

class LobbyDelegate: ServiceConnectionDelegate {
    private unowned let client: MancalaNetworkClient
    
    init(client: MancalaNetworkClient) {
        self.client = client
    }
    
    func connection(didConnect connection: ServiceConnection) {
        client.operationQueue.onDidConnectToLobby()
        client.delegate?.onDidConnectToLobby()
    }
    
    func connection(didDisconnect connection: ServiceConnection, error: Error?) {
        client.delegate?.onDidDisconnectFromLobby(withError: error)
    }
    
    func connection(_ connection: ServiceConnection, didReceiveMessage message: Message) {
        guard let type = LobbyToClientMessageType(rawValue: message.type) else {
            assert(false, "received invalid message type from lobby")
            return
        }
        
        
        switch type {
        case .createGameResponse:
            guard let payload = message.payload else {
                assert(false, "missing payload from lobby for createGameResponse message")
                return
            }
            
            let unarchiver = ByteUnarchiver(archive: payload)
            guard let response = ClientGameResponse(unarchiver: unarchiver) else {
                assert(false, "failed to unarchive ClientGameResponse message")
                return
            }
            
            switch response.result {
            case .success(let gameServerAddress, let gameToken, let gameData):
                guard client.gameToken == nil || client.gameToken == gameToken else {
                    assert(false, "received ClientGameResponse but already have a gameToken")
                    return
                }
                
                let unarchiver = ByteUnarchiver(archive: gameData)
                guard let gameInfo = MancalaGameInfo(unarchiver: unarchiver) else {
                    assert(false, "failed to unarchive MancalaGameInfo message")
                    break
                }
                
                client.operationQueue.onDidReceiveGameResponseFromLobby(withGameServerAddress: gameServerAddress, gameToken: gameToken)
                client.operationQueue.addConnectToGameServerOperation(withAddress: gameServerAddress, gameToken: gameToken)
                
                let myIdentity = client.playerIdentity(forPlayerID: gameToken.playerID)
                client.delegate?.onDidFindGame(withMyIdentity: myIdentity, opponentInfo: gameInfo.opponentInfo, startingPlayer: gameInfo.startingPlayer)
                
            case .failure(let errorText):
                client.delegate?.onDidNotFindGame(error: errorText)
                break
            }
        }
    }
    
    func connection(_ connection: ServiceConnection, didReceiveStream: StreamData) {}
}

class GameServerDelegate: ServiceConnectionDelegate {
    private unowned let client: MancalaNetworkClient
    
    init(client: MancalaNetworkClient) {
        self.client = client
    }
    
    func connection(didConnect connection: ServiceConnection) {
        guard case .connectingToGameServer(_, let gameToken) = client.operationQueue.state else {
            assert(false, "gameServer.connection(didConnect called in invalid state")
            return
        }
        
        let request = PlayerJoinGameRequest(gameToken: gameToken)
        let requestMessage = Message(type: ClientToGameServerMessageType.joinGame.rawValue, payload: ByteArchiver.archive(for: request))
        //        let gameMessage = GameMessage(gameToken: gameToken, message: requestMessage)
        //        client.operationQueue.addJoinGameServerOperation(withGameToken: gameToken, requestID: requestID, message: Message(gameMessage: gameMessage))
        client.operationQueue.addJoinGameServerOperation(withGameToken: gameToken, requestID: request.identifier, message: requestMessage)
        client.operationQueue.onDidConnectToGameServer()
        client.delegate?.onDidConnectToGameServer()
        
        client.disconnectFromLobby()
    }
    
    func connection(didDisconnect connection: ServiceConnection, error: Error?) {
        client.delegate?.onDidDisconnectFromGameServer(withError: error)
    }
    
    func connection(_ connection: ServiceConnection, didReceiveMessage message: Message) {
        guard let type = MancalaGameServerToClientMessageType(rawValue: message.type) else {
            assert(false, "received invalid message type from game server")
            return
        }
        
        switch type {
        case .gameDidStart:
            guard let didStartMessage = GameDidStartMessage(message: message) else {
                assert(false, "received invalid GameDidStartMessage type from game server")
                return
            }
            guard client.gameToken == nil || client.gameToken == didStartMessage.gameToken else {
                assert(false, "received GameDidStartMessage but already have a gameToken")
                return
            }
            
            print("*** gameToken=\(didStartMessage.gameToken)")
            
            //            let isNewGameSession = client.gameToken == nil
            client.gameToken = didStartMessage.gameToken
            log(.debug, domain, "did send GameDidStartMessage message")
            //            let playerIdentity = self.client.playerIdentity(forPlayerID: didStartMessage.gameToken.playerID)
            //            if isNewGameSession {
            //                client.delegate?.onDidFindGame(withMyPlayerIdentity: playerIdentity)
            //            }
            if case .joiningGameServer = client.operationQueue.state {
                client.operationQueue.onDidJoinGameServer()
            }
            client.delegate?.onDidStartGame(withStartingPlayer: didStartMessage.startingPlayer, gameboardID: didStartMessage.gameboardID)
            
        case .playerDidMakeMove:
            guard let didMakeMoveMessage = PlayerDidMakeMoveMessage(message: message) else {
                assert(false, "received invalid PlayerDidMakeMoveMessage type from server")
                return
            }
            
            log(.debug, domain, "did receive PlayerDidMakeMoveMessage message (pitID=\(didMakeMoveMessage.pitID), player=\(didMakeMoveMessage.playerIdentity)")
            client.delegate?.onDidMakeMove(with: didMakeMoveMessage.pitID, byPlayer: didMakeMoveMessage.playerIdentity)
            
        case .playerDidJoinGame:
            guard let didJoinGame = PlayerDidJoinGameMessage(message: message) else {
                assert(false, "received invalid PlayerDidJoinGame type from server")
                return
            }
            
            client.delegate?.onDidJoinGame(withPlayer: didJoinGame.playerIdentity)
            break
            
        case .playerDidLeaveGame:
            guard let didLeaveGame = PlayerDidLeaveGameMessage(message: message) else {
                assert(false, "received invalid PlayerDidLeaveGameMessage from server")
                return
            }
            client.delegate?.onDidLeaveGame(withPlayer: didLeaveGame.playerIdentity)
            
        case .playerDidSkipTurn:
            guard let didSkipTurn = PlayerDidSkipTurn(message: message) else {
                assert(false, "received invalid PlayerDidSkipTurn from server")
                return
            }
            client.delegate?.onDidSkipTurn(byPlayer: didSkipTurn.playerIdentity)
            //        case .playerDidTimeoutTurn:
            //            guard let didTimeoutTurn = PlayerDidTimeoutTurnMessage(message: message) else {
            //                assert(false, "received invalid PlayerDidTimeoutTurnMessage from server")
            //                return
            //            }
            //            client.delegate?.onDidTimeoutTurn(byPlayer: didTimeoutTurn.playerIdentity)
            
        case .playerDidInteractWithProp:
            guard let info = PlayerDidInteractWithPropMessage(message: message) else {
                assert(false, "received invalid PlayerDidInteractWithPropMessage from server")
                return
            }
            client.delegate?.onDidInteractWithProp(withProp: info.propID, interactionType: info.interactionType, byPlayer: info.playerIdentity)
            
        case .playerDidSendChatMessage:
            break
//            guard let payload = PlayerDidSendChatPayload(message: message) else {
//                assert(false, "received invalid PlayerDidSendChatPayload from server")
//                return
//            }
//
//            guard let chatMessage = ChatMessage(rawValue: payload.message) else {
//                assert(false, "received invalid ChatMessage from server")
//                return
//            }
//            client.delegate?.onDidReceiveChatMessage(chatMessage, fromPlayer: payload.fromPlayer)
            
        case .playerDidStartTurnCountdown:
            guard let countdown = PlayerDidStartTurnCountdown(message: message) else {
                assert(false, "received invalid PlayerDidStartTurnCountdown from server")
                return
            }
            client.delegate?.onDidStartTurnCountdown(withSeconds: countdown.seconds, player: countdown.playerIdentity)
            
        case .playerDidEarnPointsAndCoins:
            guard let earnings = PlayerDidEarnPointsAndCoins(message: message) else {
                assert(false, "received invalid PlayerDidEarnPointsAndCoins from server")
                return
            }
            client.delegate?.onDidEarn(points: earnings.pointsEarned, coins: earnings.coinsEarned)
        }
    }
    
    func connection(_ connection: ServiceConnection, didReceiveStream: StreamData) {
        
    }
}

