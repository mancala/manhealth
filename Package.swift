// swift-tools-version:3.1

import PackageDescription

let package = Package(
    name: "manhealth",
    dependencies: [
        .Package(url: "git@gitlab.com:mancala/gaming-core.git", majorVersion: 0, minor: 2),
        .Package(url: "git@gitlab.com:mancala/mancala-core.git", majorVersion: 0, minor: 1),
        .Package(url: "git@gitlab.com:mancala/mancala-lobby-core.git", majorVersion: 1, minor: 0),
        .Package(url: "git@gitlab.com:mancala/mancala-game-engine.git", majorVersion: 0, minor: 2)
    ]
)
